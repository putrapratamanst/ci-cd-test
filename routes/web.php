<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "welcome to fameo boost lumen ";
});

$router->get('generate-token', 'Auth\AuthController@generateToken');
$router->post('register', 'Auth\AuthController@register');
$router->post('login', 'Auth\AuthController@login');

$router->group(['middleware' => 'jwt.auth'], function () use ($router) {
    $router->get('/test-auth', function () use ($router) {
        return "welcome to auth ";
    });    
});