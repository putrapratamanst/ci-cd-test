<?php

/**
 * Database config
 * 
 * Please duplicate this file to use this config
 */

return [
    'default'     => 'fameo_boost',
    'migrations'  => 'migrations',
    'connections' => [
        'fameo_boost' => [
            'driver'    => env('FAMEOBOOST_DB_CONNECTION','mysql'),
            'host'      => env('FAMEOBOOST_DB_HOST', ''),
            'port'      => env('FAMEOBOOST_DB_PORT',''),
            'database'  => env('FAMEOBOOST_DB_DATABASE',''),
            'username'  => env('FAMEOBOOST_DB_USERNAME',''),
            'password'  => env('FAMEOBOOST_DB_PASSWORD'),
            'unix_socket'  => env('FAMEOBOOST_DB_SOCKET',''),
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
            'strict'    => false,
            'options'   => [
                \PDO::ATTR_EMULATE_PREPARES => true
            ],
        ],
    ],
];
