<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionChannelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_channel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('talent_id');
            $table->integer('channel_id');
            $table->string('follower');
            $table->string('engagement_rate');
            $table->string('account');
            $table->string('link');
            $table->string('sex');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('inactive_by');
            $table->timestamps();
            $table->softDeletes('inactive_at', 0);
            $table->smallInteger('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_channel');
    }
}
