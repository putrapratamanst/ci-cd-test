<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionCityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_city', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('associate_id');
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->integer('inactive_by');
            $table->timestamps();
            $table->softDeletes('inactive_at', 0);
            $table->smallInteger('status');
            $table->index(['associate_id']);	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_city');
    }
}
