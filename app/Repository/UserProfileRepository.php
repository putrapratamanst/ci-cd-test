<?php

namespace App\Repository;

use App\Models\UserProfile;

class UserProfileRepository
{
    protected $user_id;

    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }
    
    public function userByEmail(array $selectedField)
    {
        return UserProfile::select($selectedField)
            ->where('user_id', $this->user_id)
            ->whereNull('deleted_at')
            ->first();
    }
}