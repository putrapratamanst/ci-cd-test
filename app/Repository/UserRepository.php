<?php

namespace App\Repository;

use App\Models\User;

class UserRepository
{
    protected $email;

    public function setEmail($email)
    {
        $this->email = $email;
    }
    
    public function userByEmail(array $selectedField)
    {
        return User::select($selectedField)
            ->where('email', $this->email)
            ->whereNull('deleted_at')
            ->first();
    }
}