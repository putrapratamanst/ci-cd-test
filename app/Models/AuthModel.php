<?php

namespace App\Models;

class AuthModel
{
    public static $rules = [
        'register' => [
            'email'          => 'required|email',
            'password'       => 'required|min:5',
            'brandName'      => 'required|string',
            'picName'        => 'required|string',
            'picPhoneNumber' => 'required|integer',
            'city'           => 'required|string',
            'salesCode'      => 'required|string',
        ],
        'login' => [
            'username'       => 'required|email', // email user as username
            'password'       => 'required|min:5',
        ],
    ];
}