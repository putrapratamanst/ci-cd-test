<?php 

namespace App\Http\Controllers\Auth;

use App\Models\AuthModel;
use App\Traits\UserProfileTrait;
use App\Traits\UserTrait;
use App\Utilities\Exceptions\FameoBoostBaseException;
use App\Utilities\Exceptions\FameoBoostValidationParameterException;
use App\Utilities\Responses\FameoBoostApiBaseResponseBuilder;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends FameoBoostJwtAuthController
{
    use UserTrait, UserProfileTrait;

    /**
     * @var Illuminate\Http\Request
     */
    private $request;
    private $params;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->params  = $request->all();
    }
    
    /**
     * @return [array]
     * @deprecated (just for testing)
     */
    public function generateToken()
    {
        $response = new FameoBoostApiBaseResponseBuilder();

        $id = 1; //example
        $params['id'] = $id;
        $params['email'] = "fameoapptest@mailinator.com";

        $jwtToken = $this->jwt($params);
        
        $response->withStatusCode(Response::HTTP_PARTIAL_CONTENT);
        $response->withMessage("Successfully Generate Token");
        $response->withData($jwtToken);
        return $response->showResponse();
    }

    public function login()
    {
        $response = new FameoBoostApiBaseResponseBuilder();

        $validator = Validator::make($this->params, AuthModel::$rules['login']);
        if($validator->fails()){
            throw new FameoBoostValidationParameterException(implode(' | ', $validator->errors()->all()));
        }

        //check email(username) user exists
        $dataUser = $this->getDataUserByEmail(['id', 'email', 'password'], $this->params['username']);
        if(!$dataUser){
            $response->withError(true);
            $response->withStatusCode(Response::HTTP_BAD_REQUEST);
            $response->withMessage("Username Not Found, Please Try Again");
            return $response->showResponse();        
        }

        //check password
        if (!Hash::check($this->params['password'], $dataUser->password)){
            $response->withStatusCode(Response::HTTP_BAD_REQUEST);
            $response->withError(true);
            $response->withMessage("Password is Wrong, Please Try Again");
            return $response->showResponse();        
        }
        
        //generate token
        $jwtData = 
        [
            'id'    => $dataUser['id'],
            'email' => $dataUser['email']
        ];

        $jwtToken = $this->jwt($jwtData);

        $response->withStatusCode(Response::HTTP_OK);
        $response->withMessage("Successfully Generate Token");
        $response->withData($jwtToken);
        return $response->showResponse();        
    }

    public function register()
    {
        $response = new FameoBoostApiBaseResponseBuilder();

        $validator = Validator::make($this->params, AuthModel::$rules['register']);

        if($validator->fails()){
            throw new FameoBoostValidationParameterException(implode(' | ', $validator->errors()->all()));
        }

        //check email user exists
        $emailValidation = $this->getDataUserByEmail(['email'], $this->params['email']);
        if($emailValidation){
            throw new FameoBoostBaseException("Email Already Exists, Please Try Again With Another Email");
        }

        DB::beginTransaction();
        try {

            //create user
            $createUser = $this->createUser($this->params);
            if(empty($createUser)){
                DB::rollBack();
                throw new FameoBoostBaseException("Failed Create Data User");
            }

            //check user profile by user id
            $userProfileByUserId = $this->getDataUserProfileByUserId(['user_id'], $createUser->id);
            if($userProfileByUserId){
                DB::rollBack();
                throw new FameoBoostBaseException("User Already Exists, Please Check Data User");
            }
    
            //create user profile
            $this->createUserProfile($createUser->id, $this->params);
        } catch (Exception $e) {
            DB::rollBack();
            throw new FameoBoostBaseException(json_encode($e));
        }
        
        DB::commit();
        $response->withStatusCode(Response::HTTP_OK);
        $response->withMessage("Successfully Create New User");
        return $response->showResponse();        
    }
}