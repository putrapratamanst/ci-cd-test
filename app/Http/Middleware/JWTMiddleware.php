<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Auth\FameoBoostJwtAuthController;
use Closure;
use Exception;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Http\Response;

class JWTMiddleware 
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->get('token');

        //check the header
        if (empty($token)) {
            $token = trim(str_replace('Bearer', '', $request->header('Authorization')));
        }

        if (!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error'   => true,
                'message' => 'Token Not Provided.'
            ], Response::HTTP_UNAUTHORIZED);
        }

        try {            

            JWT::$leeway = 60; //add leeway 2 minute
            $credentials = JWT::decode($token, config('fameoboost.jwt_secret'), ['HS256']);

        } catch (ExpiredException $e) {
            return response()->json([
                'error'   => true,
                'message' => 'Provided Token is Expired.'
            ], Response::HTTP_UNAUTHORIZED);

            //refresh token if needed, to usage, please extends FameoBoostJwtAuthController
            //FIXME::REFRESH TOKEN
            // list($headb64, $bodyb64, $cryptob64) = explode('.', $token);
            // $payload = base64_decode($bodyb64);
            // $userData = json_decode($payload);
            // $data = [
            //     'id'    => $userData->user->id,
            //     'email' => $userData->user->email,
            // ];

            // $token = $this->jwt($data);

            // JWT::$leeway = 60; //add leeway 2 minute
            // $credentials = JWT::decode($token, config('fameoboost.jwt_secret'), ['HS256']);

            // $request->auth  = $credentials;
            // $request->Token = $token;
    
            // return $next($request);
        } catch (Exception $e) {
            return response()->json([
                'error'   => true,
                'message' => 'An error while decoding token.'
            ], Response::HTTP_BAD_REQUEST);
        }

        $request->auth  = $credentials;
        $request->Token = $token;

        return $next($request);
    }
}

