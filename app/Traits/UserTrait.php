<?php

namespace App\Traits;

use App\Models\User;
use App\Repository\UserRepository;
use Illuminate\Support\Facades\Hash;

trait UserTrait 
{
    public static function getDataUserByEmail(array $selectedField, $email)
    {
        $userRepo           = new UserRepository();
        $userRepo->setEmail($email);
        return $userRepo->userByEmail($selectedField);
    }

    public static function createUser($params)
    {
        $model           = new User();
        $model->email    = $params['email'];
        $model->password = Hash::make($params['password']);
        $model->save();

        return $model;
    }
}