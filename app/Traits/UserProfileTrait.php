<?php

namespace App\Traits;

use App\Models\UserProfile;
use App\Repository\UserProfileRepository;

trait UserProfileTrait 
{
    public static function getDataUserProfileByUserId(array $selectedField, $userId)
    {
        $userRepo            = new UserProfileRepository();
        $userRepo->setUserId($userId);
        return $userRepo->userByEmail($selectedField);
    }

    public static function createUserProfile($userId, $params)
    {
        $model                   = new UserProfile();
        $model->user_id          = $userId;
        $model->brand_name       = $params['brandName'];
        $model->pic_name         = $params['picName'];
        $model->pic_phone_number = $params['picPhoneNumber'];
        $model->city             = $params['city'];
        $model->sales_code       = $params['salesCode'];
        $model->save();
    }
}