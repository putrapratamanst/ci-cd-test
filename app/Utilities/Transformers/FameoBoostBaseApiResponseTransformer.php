<?php

namespace App\Utilities\Transformers;

use App\Utilities\Responses\FameoBoostApiBaseResponse;
use League\Fractal\TransformerAbstract;

class FameoBoostBaseApiResponseTransformer extends TransformerAbstract
{
    /**
     * @param FameoBoostApiBaseResponse $response
     * 
     * @return array
     */
    public function transform(FameoBoostApiBaseResponse $response)
    {
        $resp = [];
        $payloadDataKey = 'data';

        $resp['error'] = $response->isError();
        if ($response->getCode() != null) {
            $resp['code'] = $response->getCode();
        }
        if ($response->getMessage() != null) {
            $resp['message'] = $response->getMessage();
        }
        if (!is_null($response->getLength())) {
            $resp['length'] = $response->getLength();
        }
        if (!is_null($response->getData())) {
            $resp[$payloadDataKey] = $response->getData();
        }
        if ($response->getMessages() != null) {
            $resp['messages'] = $response->getMessages();
        }
        if ($response->getErrors() != null) {
            $resp['errors'] = $response->getErrors();
        }
        if ($response->getMeta() != null) {
            $resp['meta'] = $response->getMeta();
        }

        return $resp;
    }
}