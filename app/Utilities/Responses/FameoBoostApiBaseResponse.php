<?php

namespace App\Utilities\Responses;

class FameoBoostApiBaseResponse extends FameoBoostBaseResponse
{
    protected $code;
    protected $error;
    protected $errors;
    protected $message;
    protected $messages;
    protected $dataTransformers;
    protected $data;
    protected $dataPaginator;
    protected $meta;
    protected $length;

    /**
     * FameoBoostApiBaseResponse constructor.
     * @param bool $error
     * @param string $message
     * @param $data
     * @param $errors
     * @param $messages
     */

    public function __construct(
        $error    = false,
        $message  = "",
        $data     = null,
        $meta     = null,
        $errors   = null,
        $messages = null
    )
    {
        $this->message  = $message;
        $this->error    = $error;
        $this->errors   = $errors;
        $this->data     = $data;
        $this->messages = $messages;
        $this->meta     = $meta;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return mixed
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * @return mixed
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * @return bool
     */
    public function isError()
    {
        return $this->error;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @return null|string[]
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return null|string[]
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return [type]
     */
    public function getDataTransformers()
    {
        return $this->dataTransformers;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getDataPaginator()
    {
        return $this->dataPaginator;
    }

    /**
     * @param mixed $error
     * 
     */
    public function setError($error)
    {
        $this->error = $error;
    }
    
    /**
     * @param mixed $errors
     * 
     * @return [type]
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @param mixed $message
     * 
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @param mixed $messages
     * 
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
    }

    /**
     * @param mixed $code
     * 
     * @return [type]
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @param mixed $code
     * 
     * @return [type]
     */
    public function setDataTransformers($dataTransformers)
    {
        $this->dataTransformers = $dataTransformers;
    }

    /**
     * @param mixed $data
     * 
     * @return [type]
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param mixed $dataPaginator
     * 
     * @return [type]
     */
    public function setDataPaginator($dataPaginator)
    {
        $this->dataPaginator = $dataPaginator;
    }

    /**
     * @param mixed $meta
     * 
     */
    public function setMeta($meta)
    {
        $this->$meta = $meta;
    }

    /**
     * @param mixed $meta
     * 
     */
    public function setLength($length)
    {
        $this->$length = $length;
    }
}