<?php

namespace App\Utilities\Responses;

use App\Utilities\Serializers\FameoBoostJsonApiSerializer;
use App\Utilities\Serializers\FameoBoostJsonPaginatorApiSerializer;
use App\Utilities\Transformers\FameoBoostBaseApiResponseTransformer;
use Countable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;

/**
 * [Description FameoBoostApiBaseResponseBuilder]
 */
class FameoBoostApiBaseResponseBuilder extends AbstractFameoBoostApiBaseResponseBuilder
{
    /**
     * @var FameoBoostApiBaseResponse|null
     */
    private $response = null;

    private $statusCode = Response::HTTP_OK;

    /**
     * FameoBoostApiBaseResponseBuilder constructor
     */
    public function __construct()
    {
        $this->response = new FameoBoostApiBaseResponse();
    }

    /**
     * @param mixed $statusCode
     * 
     * @return [type]
     */
    public function withStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @param mixed $message
     * 
     * @return [type]
     */
    public function withMessage($message)
    {
        $this->response->setMessage($message);
    }

    /**
     * @param mixed $message
     * 
     * @return [type]
     */
    public function withError($error)
    {
        $this->response->setError($error);
    }

    /**
     * @param mixed $data
     * 
     * @return [type]
     */
    public function withData($data)
    {
        $this->response->setData($data);
    }

    /**
     * @param mixed $transformers
     * 
     * @return [type]
     */
    public function withDataTransformer($transformers)
    {
        $this->response->setDataTransformers($transformers);
    }

    /**
     * @param string $code
     * 
     * @return int
     */
    public function withCode($code = '') {
        $this->response->setCode($code);
    }

    /**
     * @param $errors
     */
    public function withErrors($errors)
    {
        if ($errors != null && !empty($errors)) {
            $this->response->setError(true);
        }

        $errorData = fractal()
            ->collection($errors)
            ->serializeWith(new FameoBoostJsonApiSerializer())
            ->transformWith(new FameoBoostBaseApiResponseTransformer())->toArray();
        $this->response->setErrors($errorData);
    }


    function build()
    {
        if ($this->response->getDataTransformers() != null) {
            $formattedData = $this->response->getData();

            if (is_array($this->response->getData()) || $this->response->getData() instanceof Countable || $this->response->getData() instanceof Collection) {
                if (count($this->response->getData()) > 1) {
                    if ($this->response->getDataPaginator() == null) {
                        $formattedData = fractal()
                            ->collection($formattedData)
                            ->serializeWith(new FameoBoostJsonApiSerializer())
                            ->transformWith($this->response->getDataTransformers())->toArray();
                    } else {
                        $formattedData = fractal()
                            ->collection($formattedData)
                            ->paginateWith($this->response->getDataPaginator())
                            ->serializeWith(new FameoBoostJsonPaginatorApiSerializer())
                            ->transformWith($this->response->getDataTransformers())->toArray();
                    }
                } else {
                    if ($formattedData instanceof Collection || is_array($formattedData)) {
                        if ($this->response->getDataPaginator() == null) {
                            $formattedData = fractal()
                                ->collection($formattedData)
                                ->serializeWith(new FameoBoostJsonPaginatorApiSerializer())
                                ->transformWith($this->response->getDataTransformers())->toArray();
                        } else {
                            $formattedData = fractal()
                                ->collection($formattedData)
                                ->paginateWith($this->response->getDataPaginator())
                                ->serializeWith(new FameoBoostJsonPaginatorApiSerializer())
                                ->transformWith($this->response->getDataTransformers())->toArray();
                        }
                    } else {
                        $formattedData = fractal()
                            ->item($formattedData)
                            ->serializeWith(new FameoBoostJsonApiSerializer())
                            ->transformWith($this->response->getDataTransformers())->toArray();
                    }
                }
            } else {
                $formattedData = fractal()
                    ->item($formattedData)
                    ->serializeWith(new FameoBoostJsonApiSerializer())
                    ->transformWith($this->response->getDataTransformers())->toArray();
            }

            if ((is_array($this->response->getData()) || $this->response->getData() instanceof Countable || $this->response->getData() instanceof Collection) && isset($formattedData['data'])) {
                $this->response->setData($formattedData['data']);
            } else {
                $this->response->setData($formattedData);
            }
            if (isset($formattedData['meta'])) {
                $meta = $formattedData['meta'];
                if (isset($formattedData['links'])) {
                    $meta['links'] = $formattedData['links'];
                }
                $this->response->setMeta($meta);
            }
        }
        return $this->response;
    }


    function showResponse() 
    {
        return response()->json(fractal()
            ->item($this->build())
            ->serializeWith(new FameoBoostJsonApiSerializer())
            ->transformWith(new FameoBoostBaseApiResponseTransformer())
            ->toArray(), $this->statusCode);
    }
}