<?php

namespace App\Utilities\Responses;

abstract class AbstractFameoBoostApiBaseResponseBuilder
{
    abstract function build();
    abstract function showResponse();
}