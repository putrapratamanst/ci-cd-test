<?php

namespace App\Utilities\Exceptions;

use App\Utilities\Responses\FameoBoostApiBaseErrorResponse;
use App\Utilities\Responses\FameoBoostApiBaseResponseBuilder;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class FameoBoostExceptionHandler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Exception
     */
    public function render($request, Throwable $e)
    {
        $response = new FameoBoostApiBaseResponseBuilder();
        $statusCode = Response::HTTP_BAD_REQUEST;

        if ($e instanceof FameoBoostBaseException) {
            $exceptions = $this->mapFameoBoostExceptionToApiErrorResponse($e);
        } elseif ($e instanceof NotFoundHttpException) {
            $statusCode = Response::HTTP_NOT_FOUND;
            $exceptions = $this->mapNotFoundHttpExceptionToApiErrorResponse();
        } else {
            $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
            $exceptions = $this->mapExceptionToApiErrorResponse($e);
        }

        if ($e instanceof FameoBoostUnauthorizedException || $e instanceof AuthorizationException) {
            $statusCode = Response::HTTP_UNAUTHORIZED;
        }

        $errors = $exceptions;
        // //handle for message and code first
        if (sizeof($errors) > 0) {
            $error = reset($errors);
            if (!empty($error)) {
                $response->withCode($error->getErrorCode());
                $response->withMessage($error->getErrorMessage());
            }
        }

        //show all errors

        $response->withError(true);
        $response->withStatusCode($statusCode);

        return $response->showResponse();
    }

    private function mapFameoBoostExceptionToApiErrorResponse(FameoBoostBaseException $e)
    {
        $listOfErrors = array();
        if ($e instanceof FameoBoostUnauthorizedException) {
            $error = new FameoBoostApiBaseErrorResponse("AUTH.0001", "Username or Password is wrong.");
            array_push($listOfErrors, $error);
        } else {
            $error = new FameoBoostApiBaseErrorResponse($e->getErrorCode(), $e->getErrorMessage());
            array_push($listOfErrors, $error);
        }

        return $listOfErrors;
    }

    private function mapNotFoundHttpExceptionToApiErrorResponse()
    {
        $listErrors = [];
        $error = new FameoBoostApiBaseErrorResponse('EXCEPT.404', 'URL Not Found, please check request and log');
        array_push($listErrors, $error);        
        return $listErrors;
    }

    private function mapExceptionToApiErrorResponse(Throwable $e)
    {
        $listErrors = array();
        $error = new FameoBoostApiBaseErrorResponse("EXCPT.500", $e->getMessage());
        array_push($listErrors, $error);

        return $listErrors;
    }    
}
