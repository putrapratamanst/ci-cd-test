<?php

namespace App\Utilities\Exceptions;

use Throwable;

class FameoBoostValidationParameterException extends FameoBoostBaseException
{
    public function __construct(
        $errorMessage,
        $errorCode = "FAMEO BOOST VALIDATION.0001",
        $code = 0,
        Throwable $previous = null
    )
    {
        parent::__construct($errorMessage, $errorCode, $code, $previous);
    }
}