<?php

namespace App\Utilities\Exceptions;

use Exception;
use Throwable;

class FameoBoostBaseException extends Exception
{
    protected $errorCode = "";
    protected $errorMessage = [];

    public function __construct(
        $errorMessage,
        $errorCode = "FAMEO BOOST EXCEPTION.001",
        $code = 0,
        Throwable $previous = null
    )
    {
        parent::__construct($errorMessage, $code, $previous);
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
}