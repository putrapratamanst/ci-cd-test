FROM php:7.4-apache-buster as builder

COPY . /var/www/html
WORKDIR /var/www/html

RUN a2enmod rewrite

RUN cp .env-dist .env

RUN rm -rf storage/logs && \
    mkdir -p storage/logs && \
    chmod -R 777 storage/logs

RUN rm -rf storage/app && \
    mkdir -p storage/app && \
    chmod -R 777 storage/app

RUN apt update
RUN apt-get install -y curl openssh-server openssh-client git libpng-dev zlib1g-dev libzip-dev libmcrypt-dev zip wget

COPY apache-config.conf /etc/apache2/sites-enabled/000-default.conf
COPY opcache.ini /usr/local/etc/php/conf.d/opcache.ini

RUN sed -i 's/80/${PORT}/g' /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf

# install composer
RUN curl -sS https://getcomposer.org/installer -o /tmp/composer-setup.php \
  && php /tmp/composer-setup.php --install-dir=/usr/local/bin --filename=composer \
  && rm -rf /var/cache/apk/*

#setup ssh
# RUN mkdir -p /root/.ssh && \
#     chmod 700 /root/.ssh && \
#     ssh-keyscan bitbucket.org > /root/.ssh/known_hosts

# RUN echo "$BASE64_PRIVATE_KEY" | base64 -d > /root/.ssh/id_rsa && \
#     chmod 700 /root/.ssh/id_rsa

# setup php modules
RUN docker-php-ext-configure pdo_mysql && \
    docker-php-ext-configure opcache && \
    docker-php-ext-install pdo_mysql opcache

RUN composer install \
    &&  composer dumpautoload -o

EXPOSE ${PORT}

# set env on the fly (todo) https://cloud.google.com/run/docs/configuring/environment-variables

ENV APP_ENV=${APP_ENV}
ENV APP_DEBUG=${APP_DEBUG}
ENV APP_KEY=${APP_KEY}
ENV APP_TIMEZONE=${APP_TIMEZONE}

ENV FAMEOBOOST_DB_CONNECTION=${FAMEOBOOST_DB_CONNECTION}
ENV FAMEOBOOST_DB_HOST=${FAMEOBOOST_DB_HOST}
ENV FAMEOBOOST_DB_PORT=${FAMEOBOOST_DB_PORT}
ENV FAMEOBOOST_DB_DATABASE=${FAMEOBOOST_DB_DATABASE}
ENV FAMEOBOOST_DB_USERNAME=${FAMEOBOOST_DB_USERNAME}
ENV FAMEOBOOST_DB_PASSWORD=${FAMEOBOOST_DB_PASSWORD}
ENV FAMEOBOOST_DB_SOCKET=${FAMEOBOOST_DB_SOCKET}

ENV CACHE_DRIVER=${CACHE_DRIVER}
ENV QUEUE_DRIVER=${QUEUE_DRIVER}

ENV LOG_LEVEL=${LOG_LEVEL}

ENV JWT_SECRET_KEY=${JWT_SECRET_KEY}


# RUN wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64 -O cloud_sql_proxy \
#   && chmod +x cloud_sql_proxy \
#   && chmod +x run.sh

# CMD ./cloud_sql_proxy -instances=versatile-age-284708:asia-southeast2:fameoboostdatabase=tcp:0.0.0.0:3306